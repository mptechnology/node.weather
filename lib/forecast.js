/**
* mpWeather Forecast.js
* copyright (c) 2012 MP Technology Group <code@mptechnologygroup.com>
*
* MIT Licensed
*
* This module will pull data from the National Weather Service, 
* a US Government Agency. This data is free of charge and public domain 
* unlike many of the other for-profit weather API, many of which also 
* use NWS data.
*
* More Info on  NOAA can be found at www.noaa.org
*
**/

var Forecast = function() {
	
	var self				= this;	

	self.areaDisription 	=  	'';
	self.weatherLink		=	'';
	self.timeLayouts  		=	[];
	self.tempatures			= {
		high: 		[],
		low: 		[] 	
	};
	self.precipitation		= 	[];
	self.summary   			=	[]; 
	self.noaaicons			=	[];
	self.wordedforecast		=	[];

	this.get3Day = function(){

		var values = [];
		for ($i=0; $i < 3; $i++ ){
			var day = {};

			var hour12index 		= $i;
			var hour24dayindex		= self.timeLayouts[0].periods.indexOf(self.timeLayouts[1].periods[$i]);
			var hour24nightindex	= self.timeLayouts[0].periods.indexOf(self.timeLayouts[2].periods[$i]);

			day.name = self.timeLayouts[1].periods[$i];
			day.highTemp = self.tempatures.high[$i];
			day.lowTemp = self.tempatures.low[$i];
			day.precipitation.daytime = self.precipitation[hour24dayindex];
			day.precipitation.nighttime = self.precipitation[hour24nightindex];
			day.summary.daytime = self.summary[hour24dayindex];
			day.summary.nighttime = self.summary[hour24nightindex];
			day.noaaicons.daytime = self.noaaicons[hour24dayindex];
			day.noaaicons.nighttime = self.noaaicons[hour24nightindex];
			day.wordedforecast.daytime = self.wordedforecast[hour24dayindex];
			day.wordedforecast.nighttime = self.wordedforecast[hour24nightindex];

			values.push(day);
		}

		return values;
	}


	this.getForecast = function(){

		var values = [];

		count = self.timeLayouts[0].periods.length + 1;

		for ($i=0; $i < count; $i++ ){
			var day = {

			};

			var hour24index 		= $i;
			var temparray 			= self.timeLayouts[1].periods.indexOf(self.timeLayouts[0].periods[$i]) > 0 ? 'high' : 'low';
			var hour12index			= self.timeLayouts[1].periods.indexOf(self.timeLayouts[0].periods[$i]) > 0 ? self.timeLayouts[1].periods.indexOf(self.timeLayouts[0].periods[$i]) : self.timeLayouts[2].periods.indexOf(self.timeLayouts[0].periods[$i]);
			
			console.log(self.timeLayouts[0].periods[$i]);
			console.log(temparray);
			console.log(hour12index);
			values.push({
				name: self.timeLayouts[0].periods[$i],
				temp: self.tempatures[temparray][hour12index],
				precipitation: self.precipitation[$i],
				summary: self.summary[$i],
				noaaicons: self.noaaicons[$i],
				wordedforecast: self.wordedforecast[$i]
			});

		}

		return values;

	}

	this.processForecast = function(jsondata,weathere){
		
		timeLayouts = {};

		jsondata.data[0]['time-layout'].forEach(function(element,index){
			
			var timeLayout 	= {
				layoutKey: 	element['layout-key'],
				periods: 	[],
				datetimes: 	[]
			};

			element['start-valid-time'].forEach(function(element,index){
				timeLayout.periods.push(element['@']['period-name']);
				timeLayout.datetimes.push(element['#']);
				
			});

			forecast.timeLayouts.push(timeLayout);
		});
		
		jsondata.data[0]['parameters']['temperature'].forEach(function(element,index){

			if(element['@']['type'] === 'minimum'){
				
				element['value'].forEach(function(element,index){
					forecast.tempatures.low.push(element);
				});

			}else{
				element['value'].forEach(function(element,index){
					forecast.tempatures.high.push(element);
				});
			}

		});

		jsondata.data[0]['parameters']['weather']['weather-conditions'].forEach(function(element,index){
			forecast.summary.push(element['@']['weather-summary']);
		});

		jsondata.data[0]['parameters']['conditions-icon']['icon-link'].forEach(function(element,index){
			forecast.noaaicons.push(element);
		});

		jsondata.data[0]['parameters']['wordedForecast']['text'].forEach(function(element,index){
			forecast.wordedforecast.push(element);
		});

		var value = '';
		jsondata.data[0]['parameters']['probability-of-precipitation']['value'].forEach(function(element,index){
			if( ! element['@']){
				value = element;
			}
			forecast.precipitation.push(value);
		});

		console.log('Emitting Weather Update: '+weathere);
		self.emit(weathere,forecast);

	}

};



module.exports = new Forecast();