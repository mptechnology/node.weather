#THIS MODULE IS NOT COMPLETE 
#NOT READY FOR PRODUCTION

#NodeJS Weather Module

This module will pull data from the National Weather Service, a US Government Agency.
This data is free of charge and public domain unlike many of the other for-profit weather API, many of which also use NWS data. 

##Usage

To be Written

##Events
noLocation:  If forecast is requested with no Lat / Lon defined this event is triggered,  see noLocation listener for internal processing of this event
forecastUpdated: Once the Forecast data from NOAA is parsed and put in to a JS Object this event is fired,  your app should listen for this event to update its use of the data 
currentWeatherUpdated: Once the current station OBS data is processed and put into a JS Object this event if fired, your app should listen for this even to update its use of the data
error: Error is currently on used if an HTTP error occurs when contacting the NOAA servers. 

##Listeners
noLocation: No Location is only fired ONCE. when fired the module will trigger get current station, which should include the lat and lon of that station to enable the retrival of forecast data. Subsequent noLocation events will be ingored by the module,  it is up to you to program a listent to error out.
updateCurrentWeather: When this event is fired the current weather data will be updated 
updateForecast: when this event is fired fired the current forecast will be updated

##Licence

Released under the MIT Lic. 

##Author

MP Technology Group <code@mptechnologygroup.com>