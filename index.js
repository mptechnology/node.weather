/**
* mpWeather 
* copyright (c) 2012 MP Technology Group <code@mptechnologygroup.com>
*
* MIT Licensed
*
* This module will pull data from the National Weather Service, 
* a US Government Agency. This data is free of charge and public domain 
* unlike many of the other for-profit weather API, many of which also 
* use NWS data.
*
* More Info on  NOAA can be found at www.noaa.org
*
**/

/**
 * Module dependencies.
 */

var util = require('util'),
	events = require('events'),
	http = require('http'),
	fs = require('fs'),
	xml2js = require('xml2js');

	

/**
* Create the main Weather Object
*/


var Weather = function (){

	var self			= this;	

	self.station 		= 'KNYC'; //NOAA Sttion ID, Station ID's can be found at http://w1.weather.gov/xml/current_obs/
	self.lat			= 40.43;  //GPS Latitude of any Point in the USA
	self.lon			= -86.93; //GPS Longitude of any Point in the USA
	self.forecast 		= require('./lib/forecast'); //Load Forecast Data Model
	self.current_obs	= null; //Current Conditions for the Station
	self.responsed 		= ''; //Raw Response Data from NOAA
	self.debug			= false;
	events.EventEmitter.call(this); //Setup events


	this.init = function (station){
		
		if(typeof station === 'undefined'){
			type = 'KNYC';
		}

		self.station = station;

		self.getWeather('current');
	}


	/**
	* Get Weather from NOAA,  types are "current" for current Conditions or "forecast"
	*  
	*/

	this.getWeather = function (type){
		
		if(typeof type === 'undefined'){
			type = 'current';
		}
		switch (type){
			case 'current':
				url = 'http://w1.weather.gov/xml/current_obs/'+self.station+'.xml';
				weathere = 'currentWeatherUpdated';
			break;
			case 'forecast':
				if( ! self.lat ||  ! self.lat  ){
					if(self.debug) console.log('Emitting NoLocation Event');
					self.emit('noLocation');
					return;
				}
				
				url = 'http://forecast.weather.gov/MapClick.php?textField1='+self.lat+'&textField2='+self.lon+'&FcstType=dwml';
				weathere = 'forecastUpdated';
			break;
			default:
				if(self.debug) console.log('No Weather Type Selected');
				return;
			break;

		}
		if(self.debug) console.log(url);
		http.get(url, function(res){
			if(self.debug) console.log('Response Received from Weather.gov :'+ res.statusCode);
			var httpdata = ''
			if(res.statusCode === 200){
				res.on('data', function(data){
					if(self.debug) console.log('Data Chunk Passed to data var');
					httpdata = httpdata + data 
				});

				res.on('end', function(){
					if(self.debug) console.log('HTTP Get Complete, Passing Data to processWeatherXML');
					processWeatherXML(httpdata,weathere);
				});
			}else{
				if(self.debug) console.log('HTTP Error: '+station+' - Status:'+ res.statusCode);
				self.emit('error');
			}
		});
	}



	var processWeatherXML = function(xml,weathere){
		
		var parser = new xml2js.Parser();
		console.log('Parsing XML Data')
		
		parser.parseString(xml,function(err,result){

			if(err){
				console.log('Parser Error: '+ err);
				return;
			}

			if(result && result.station_id){
				console.log('setting Lat '+result.latitude)
				self.lat = result.latitude;
				console.log('setting Lon '+result.longitude)
				self.lon = result.longitude;
				console.log('Emitting Current Weather Update: '+weathere);
				self.emit(weathere,result);
				return;
			}

			console.log('Passing to processForecast');
			forecast.processForecast(result,weathere);
			
		});
	}
	
	self.on('noLocation', function(){
		console.log('Setting Once Event');
		self.once('currentWeatherUpdated', function(){ 
			
			console.log(self.lat + '-' +self.lon);

			self.getWeather('forecast');
		});
		self.getWeather('current');
	});

	self.on('updateCurrentWeather', function(){
		self.getWeather('current');
	});

	self.on('updateForecast', function(){
		self.getWeather('forecast');
	});
}

util.inherits(Weather, events.EventEmitter);

module.exports = new Weather();
